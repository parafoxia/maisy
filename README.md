# Maisy

An emotive AI chatbot in Python (dissertation project).

## Running instructions

1. Make sure Docker is installed on the VM.
2. `cd` into this directory in a terminal.
3. Run `docker build -t maisy .`.
4. Wait for the image to build. This will probably take around 10 minutes.
5. Run `docker run --gpus all -it --rm -v $PWD:/tmp -w /tmp maisy python ./scripts/train_chat_model.py`.
6. Hope it works!

If there's an error, let me know. If it comes up with an enormous error about resources, reduce the size of the dataset until it works (let me know what size you needed to make it for it to run).
