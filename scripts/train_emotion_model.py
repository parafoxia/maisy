# Maisy - An emotive AI chatbot.
# Copyright (C) 2021  Ethan Henderson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Ethan Henderson
# 15619184@students.lincoln.ac.uk

import argparse
from pathlib import Path

import pandas as pd
import tensorflow as tf
from tensorflow.keras.layers.experimental import preprocessing

from utils import make, patch
from utils.models import EmotionModel

DS_PATH = Path("./data/datasets")
MODEL_PATH = Path("./data/models")


def create_datasets(*ds_names):
    def prepare(ds_name):
        df = pd.read_csv(DS_PATH / ds_name, index_col=0)
        feature = df["text"]
        labels = df[["V", "A", "D"]]
        ds = tf.data.Dataset.from_tensor_slices((feature.values, labels.values))
        return ds.shuffle(opts.buffer_size).batch(opts.batch_size).prefetch(tf.data.experimental.AUTOTUNE)

    return [prepare(name) for name in ds_names]


def create_text_encoder(ds):
    encoder = preprocessing.TextVectorization()
    encoder.adapt(ds.map(lambda text, label: text))
    return encoder


def create_model(text_encoder):
    model = EmotionModel(text_encoder, nodes=opts.nodes)
    model.compile(
        loss=tf.keras.losses.MeanSquaredError(),
        optimizer=tf.keras.optimizers.Adam(1e-4),
    )
    return model


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Train an emotion model.")
    parser.add_argument("-e", "--epochs", help="The number of epochs to train.", type=int)
    parser.add_argument("-n", "--nodes", help="The number of nodes to use (default: 64).", type=int, default=64)
    parser.add_argument("--batch-size", help="The batch size (default: 64).", type=int, default=64)
    parser.add_argument("--buffer-size", help="The buffer size (default: 10,000).", type=int, default=10_000)
    opts = parser.parse_args()

    if not opts.epochs:
        raise ValueError("you must specify a number of epochs")

    make.data_dirs()
    patch.gpu()

    train_ds, test_ds = create_datasets("clean_train.csv", "clean_test.csv")
    text_encoder = create_text_encoder(train_ds)
    model = create_model(text_encoder)

    with tf.device("/cpu:0"):
        history = model.fit(train_ds, epochs=opts.epochs, validation_data=test_ds, validation_steps=5)
    test_loss = model.evaluate(test_ds)
    print(f"Test loss: {test_loss}")
    model.save(MODEL_PATH / f"ME{opts.epochs}N{opts.nodes}XS")
    print(f"Model saved. Check {MODEL_PATH.resolve()}.")
