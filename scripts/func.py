# Maisy - An emotive AI chatbot.
# Copyright (C) 2021  Ethan Henderson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Ethan Henderson
# 15619184@students.lincoln.ac.uk

import re
import unicodedata
from pathlib import Path

import tensorflow as tf

DS_PATH = Path("./data/datasets")

BATCH_SIZE = 64


def convert_to_ascii(text):
    return "".join(c for c in unicodedata.normalize("NFD", text) if unicodedata.category(c) != "Mn")


def preprocess_text(text):
    text = text.replace(" NEWLINECHAR ", " ").replace("’", "").replace("'", "")
    text = text.lower().strip()
    text = re.sub(r"([?.!,])", r" \1 ", text)
    text = re.sub(r'[" "]+', " ", text)
    text = re.sub(r"[^a-zA-Z0-9?.!,]+", " ", text)
    return f"<start> {text.strip()} <end>"


def load_pairs_from(ds_name):
    with open(DS_PATH / ds_name, "r", encoding="utf-8") as f:
        inputs, outputs = zip(*[(preprocess_text(part) for part in l.split("\t")) for l in f])
    return inputs, outputs


def create_tokeniser_from(ds_name):
    with open(DS_PATH / ds_name, "r", encoding="utf-8") as f:
        inputs, outputs = zip(*[(preprocess_text(part) for part in l.split("\t")) for l in f])
    samples = inputs + outputs

    tokeniser = tf.keras.preprocessing.text.Tokenizer(filters="")
    tokeniser.fit_on_texts(samples)
    input_tensor = tokeniser.texts_to_sequences(inputs)
    input_tensor = tf.keras.preprocessing.sequence.pad_sequences(input_tensor, padding="post")
    output_tensor = tokeniser.texts_to_sequences(outputs)
    output_tensor = tf.keras.preprocessing.sequence.pad_sequences(output_tensor, padding="post")
    return tokeniser, input_tensor, output_tensor


def create_dataset(input_tensor, output_tensor):
    ds = tf.data.Dataset.from_tensor_slices((input_tensor, output_tensor))
    return ds.shuffle(len(input_tensor)).batch(BATCH_SIZE, drop_remainder=True)
