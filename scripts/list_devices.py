import tensorflow.config as tfc

if __name__ == "__main__":
    for d in tfc.list_physical_devices():
        print(f"{d.device_type}: {d.name}")
