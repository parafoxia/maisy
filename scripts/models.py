# Maisy - An emotive AI chatbot.
# Copyright (C) 2021  Ethan Henderson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Ethan Henderson
# 15619184@students.lincoln.ac.uk

__all__ = ["EmotionModel", "ChatEncoder", "ChatDecoder"]

import tensorflow as tf
from tensorflow.keras.layers.experimental import preprocessing


class BahdanauAttention(tf.keras.layers.Layer):
    def __init__(self, *, encoding_units):
        super().__init__()
        self.W1 = tf.keras.layers.Dense(encoding_units)
        self.W2 = tf.keras.layers.Dense(encoding_units)
        self.V = tf.keras.layers.Dense(1)

    def call(self, query, values):
        query_with_time_axis = tf.expand_dims(query, 1)
        score = self.V(tf.nn.tanh(self.W1(query_with_time_axis) + self.W2(values)))
        attention_weights = tf.nn.softmax(score, axis=1)
        context_vector = attention_weights * values
        context_vector = tf.reduce_sum(context_vector, axis=1)
        return context_vector, attention_weights


class EmotionModel(tf.keras.Model):
    def __init__(self, text_encoder, *, nodes):
        super().__init__()
        self.encoder = text_encoder
        self.embedding = tf.keras.layers.Embedding(
            input_dim=len(text_encoder.get_vocabulary()), output_dim=nodes, mask_zero=True
        )
        self.bidirectional1 = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(nodes, return_state=True, return_sequences=True),
            backward_layer=tf.keras.layers.LSTM(nodes, go_backwards=True, return_state=True, return_sequences=True),
        )
        self.bidirectional2 = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(nodes, return_state=True, return_sequences=True),
            backward_layer=tf.keras.layers.LSTM(nodes, go_backwards=True, return_state=True, return_sequences=True),
        )
        self.bidirectional3 = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(nodes),
            backward_layer=tf.keras.layers.LSTM(nodes, go_backwards=True),
        )
        self.dense1 = tf.keras.layers.Dense(nodes, kernel_initializer="lecun_normal", activation="selu")
        self.dense2 = tf.keras.layers.Dense(nodes, kernel_initializer="lecun_normal", activation="selu")
        self.dense3 = tf.keras.layers.Dense(nodes, kernel_initializer="lecun_normal", activation="selu")
        self.dense_output = tf.keras.layers.Dense(3, kernel_initializer="lecun_normal", activation="selu")

    def call(self, inputs):
        x = self.encoder(inputs)
        x = self.embedding(x)
        x = self.bidirectional1(x)
        x = self.bidirectional2(x)
        x = self.bidirectional3(x)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        return self.dense_output(x)


class ChatEncoder(tf.keras.Model):
    def __init__(self, vocab_size, *, batch_size, embedding_dimensions, encoding_units):
        super().__init__()
        self.batch_size = batch_size
        self.encoding_units = encoding_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dimensions)
        self.gru = tf.keras.layers.GRU(encoding_units, return_sequences=True, return_state=True, recurrent_initializer="glorot_uniform")

    def call(self, x, hidden):
        x = self.embedding(x)
        output, state = self.gru(x, initial_state=hidden)
        return output, state

    def initialize_hidden_state(self):
        return tf.zeros((self.batch_size, self.encoding_units))


class ChatDecoder(tf.keras.Model):
    def __init__(self, vocab_size, *, embedding_dimensions, encoding_units):
        super().__init__()
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dimensions)
        self.gru = tf.keras.layers.GRU(encoding_units, return_sequences=True, return_state=True, recurrent_initializer="glorot_uniform")
        self.fc = tf.keras.layers.Dense(vocab_size)
        self.attention = BahdanauAttention(encoding_units=encoding_units)

    def call(self, x, hidden, encoder_output):
        context_vector, attention_weights = self.attention(hidden, encoder_output)
        x = self.embedding(x)
        x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)
        output, state = self.gru(x)
        output = tf.reshape(output, (output.shape[0], output.shape[1] * output.shape[2]))
        x = self.fc(output)
        return x, state, attention_weights
