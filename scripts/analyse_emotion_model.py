# Maisy - An emotive AI chatbot.
# Copyright (C) 2021  Ethan Henderson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Ethan Henderson
# 15619184@students.lincoln.ac.uk

import logging
import os
import statistics as stats
import sys
from pathlib import Path

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from beautifultable import BeautifulTable

tf.get_logger().setLevel(logging.ERROR)

MODEL = sys.argv[1]
ANALYSIS_PATH = Path("./data/analyses")
DS_PATH = Path("./data/datasets")
MODEL_PATH = Path("./data/models")


def plot_data(data):
    plt.figure()
    box = sns.boxenplot(data=data, x="Dimension", y="Value", hue="Type")
    plt.savefig(ANALYSIS_PATH / f"{MODEL}/data-analysis.png")


def plot_trend(data):
    plt.figure()
    sns.histplot(data=data, x="Value", hue="Type", bins=20, stat="probability", element="poly")
    plt.savefig(ANALYSIS_PATH / f"{MODEL}/trend-analysis.png")


def create_data_table(data, col):
    truth = data[data["Type"] == "Truth"]["Value"].values
    preds = data[data["Type"] == "Pred"]["Value"].values
    diffs = np.abs(truth - preds)

    table = BeautifulTable()

    table.rows.append(
        [
            tmin := np.round(np.min(truth), 3),
            tmax := np.round(np.max(truth), 3),
            np.round(tmax - tmin, 3),
            np.round(np.mean(truth), 3),
            np.round(np.median(truth), 3),
        ]
    )
    table.rows.append(
        [
            pmin := np.round(np.min(preds), 3),
            pmax := np.round(np.max(preds), 3),
            np.round(pmax - pmin, 3),
            np.round(np.mean(preds), 3),
            np.round(np.median(preds), 3),
        ]
    )
    table.rows.append(
        [
            dmin := np.round(np.min(diffs), 3),
            dmax := np.round(np.max(diffs), 3),
            np.round(dmax - dmin, 3),
            np.round(np.mean(diffs), 3),
            np.round(np.median(diffs), 3),
        ]
    )
    table.columns.header = ("Min", "Max", "Range", "Mean", "Median")
    table.rows.header = ("Truth", "Pred", "Diff")
    return table


def plot_predictions(data, col):
    os.makedirs(ANALYSIS_PATH / (path := f"{MODEL}/predictions/"), exist_ok=True)
    plt.figure()
    sns.scatterplot(data=data, x="Observation", y="Value", hue="Type", s=5)
    sns.rugplot(data=data, y="Value", hue="Type", lw=1, alpha=0.05)
    plt.savefig(ANALYSIS_PATH / f"{path}/{col}.png")


def plot_errors(data, col):
    os.makedirs(ANALYSIS_PATH / (path := f"{MODEL}/errors/"), exist_ok=True)
    truth = data[data["Type"] == "Truth"]["Value"].values
    preds = data[data["Type"] == "Pred"]["Value"].values
    x = range(np.max(data["Observation"].values) + 1)
    y = np.abs(truth - preds)

    plt.figure()
    grid = sns.JointGrid()
    sns.histplot(x=x, y=y, ax=grid.ax_joint)
    sns.rugplot(y=y, lw=1, expand_margins=False, ax=grid.ax_joint)
    sns.lineplot(x=x, y=y, ax=grid.ax_marg_x)
    sns.kdeplot(y=y, ax=grid.ax_marg_y)
    grid.ax_joint.set_xlabel("Observation")
    grid.ax_joint.set_ylabel("Error")
    plt.savefig(ANALYSIS_PATH / f"{path}/{col}.png")


def analyse_model(model, ds_name):
    print("Preparing...")
    os.makedirs(ANALYSIS_PATH / MODEL, exist_ok=True)
    df = pd.read_csv(DS_PATH / ds_name, index_col=0)
    df_len = len(df)
    predictions = model.predict(np.array([s]) for s in df["text"])

    dft = pd.DataFrame(
        [[i, col, "Truth", df[col][i]] for col in df.iloc[:, 1:] for i in range(len(df))],
        columns=["Observation", "Dimension", "Type", "Value"],
    )
    dfp = pd.DataFrame(
        [[j, col, "Pred", predictions[j, i]] for i, col in enumerate(df.iloc[:, 1:]) for j in range(len(df))],
        columns=["Observation", "Dimension", "Type", "Value"],
    )
    dfx = pd.concat([dft, dfp], axis=0, ignore_index=True)

    print("Analysing. Please wait...")
    tables: t.Dict[str, BeautifulTable] = {}
    plot_data(dfx)
    plot_trend(dfx)

    for col in df.iloc[:, 1:]:
        data = dfx[dfx["Dimension"] == col]
        tables.update({col: create_data_table(data, col)})
        plot_predictions(data, col)
        plot_errors(data, col)

    with open(ANALYSIS_PATH / f"{MODEL}/table-analysis.txt", "w", encoding="utf-8") as f:
        for col, table in tables.items():
            f.write(f"Column {col}:\n{table}\n\n")
    print(f"Analysis complete. Check {ANALYSIS_PATH.resolve()}.")


print(f"Loading {MODEL} model...")
with tf.device("/cpu:0"):
    model = tf.keras.models.load_model(MODEL_PATH / MODEL)
    analyse_model(model, "clean_test.csv")
