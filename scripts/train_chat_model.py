# Maisy - An emotive AI chatbot.
# Copyright (C) 2021  Ethan Henderson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed out the hope that out will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Ethan Henderson
# 15619184@students.lincoln.ac.uk

import time
from pathlib import Path

import func
import make
import models
# import patch
import tensorflow as tf
from sklearn.model_selection import train_test_split

gpus = tf.config.list_physical_devices("GPU")
for gpu in gpus:
    try:
        tf.config.experimental.set_memory_growth(gpu, enable=True)
        # tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024 * 8)
    except:
        pass

CHECKPOINT_PATH = Path("./data/models")

BATCH_SIZE = 64
EMBEDDING_DIMS = 1024
ENC_UNITS = 1024

EPOCHS = 100


def loss_function(truth, predictions):
    mask = tf.math.logical_not(tf.math.equal(truth, 0))
    loss = scc_loss(truth, predictions)
    mask = tf.cast(mask, dtype=loss.dtype)
    loss *= mask
    return tf.reduce_mean(loss)


@tf.function
def train_step(input_, output, encoder_hidden):
    loss = 0

    with tf.GradientTape() as tape:
        encoder_output, encoder_hidden = encoder(input_, encoder_hidden)
        decoder_hidden = encoder_hidden
        decoder_input = tf.expand_dims([tokeniser.word_index["<start>"]] * BATCH_SIZE, 1)

        for t in range(1, output.shape[1]):
            predictions, decoder_hidden, _ = decoder(decoder_input, decoder_hidden, encoder_output)
            loss += loss_function(output[:, t], predictions)
            decoder_input = tf.expand_dims(output[:, t], 1)

        batch_loss = loss / int(output.shape[1])
        variables = encoder.trainable_variables + decoder.trainable_variables
        gradients = tape.gradient(loss, variables)
        optimiser.apply_gradients(zip(gradients, variables))
        return batch_loss


if __name__ == "__main__":
    make.data_dirs()
    # patch.gpu()

    tokeniser, input_tensor, output_tensor = func.create_tokeniser_from("clean_chat.txt")
    input_train_tensor, input_test_tensor, output_train_tensor, output_test_tensor = train_test_split(
        input_tensor, output_tensor, test_size=0.25
    )
    input_length = input_tensor.shape[1]
    output_length = output_tensor.shape[1]

    ds = func.create_dataset(input_train_tensor, output_train_tensor)

    steps_per_epoch = len(input_train_tensor) // BATCH_SIZE
    vocab_size = len(tokeniser.word_index) + 1

    encoder = models.ChatEncoder(vocab_size, batch_size=BATCH_SIZE, embedding_dimensions=EMBEDDING_DIMS, encoding_units=ENC_UNITS)
    attention = models.BahdanauAttention(encoding_units=ENC_UNITS)
    decoder = models.ChatDecoder(vocab_size, embedding_dimensions=EMBEDDING_DIMS, encoding_units=ENC_UNITS)
    optimiser = tf.keras.optimizers.Adam()
    scc_loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction="none")
    checkpoint = tf.train.Checkpoint(optimizer=optimiser, encoder=encoder, decoder=decoder)

    # with tf.device("/gpu:0"):
    for epoch in range(EPOCHS):
        start = time.time()

        encoder_hidden = encoder.initialize_hidden_state()
        total_loss = 0

        for (batch, (input_, output)) in enumerate(ds.take(steps_per_epoch)):
            batch_loss = train_step(input_, output, encoder_hidden)
            total_loss += batch_loss

            # if batch % 100 == 0:
            print(f"Epoch {epoch + 1} Batch {batch + 1} Loss {batch_loss.numpy():.4f}")

        # if (epoch + 1) % 2 == 0:
        checkpoint.save(file_prefix=CHECKPOINT_PATH / f"MC{EPOCHS}E{EMBEDDING_DIMS}/x")

        print(f"Epoch {epoch+1} Loss {total_loss/steps_per_epoch:.4f}")
        print(f"Time taken for 1 epoch {time.time()-start} sec(s)\n")

    print("Done!")
